#import <Foundation/Foundation.h>

@class MyAppStore;
@class MyCategory;


@interface MyApplication : NSObject {
	
	NSString * name;
	
	NSInteger sizeKB;
	
	MyAppStore * appStore;
	
	MyCategory * category;
	
}

	@property (assign) NSString * name;

	@property (assign) NSInteger sizeKB;

	@property (retain) MyAppStore * appStore;

	@property (retain) MyCategory * category;



@end