#import <Foundation/Foundation.h>

@class MyApplication;


@interface MyAppStore : NSObject {
	
	NSString * name;
	
	NSArray * applications;
	
}

	@property (assign) NSString * name;

	@property (retain) NSArray * applications;



	- (NSInteger)calculateAppCount;

@end