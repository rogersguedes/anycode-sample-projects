#import <Foundation/Foundation.h>
#import "MyApplication.h"
#import "MyAppStore.h"
#import "MyCategory.h"

int main (int argc, const char *argv[]) {                          
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];    
    
    MyAppStore * appStore = [[MyAppStore alloc] init];
    NSMutableArray * apps = [[NSMutableArray alloc] init];
    [appStore setApplications:apps]; 
    
    MyApplication * myApp = [[MyApplication alloc] init];
    MyCategory * cat1 = [[MyCategory alloc] init];
    cat1.name = @"Tools";
	myApp.name = @"FirstApp!";
	myApp.sizeKB = 7980;
	[myApp setCategory:cat1];
	[apps addObject: myApp];
    
	myApp = [[MyApplication alloc] init];
    myApp.name = @"SecondApp!";
	myApp.sizeKB = 6420;
	[myApp setCategory:cat1];
	[apps addObject: myApp];
    
	
    
    
   

    
    NSLog(@"Your app name is %@ and it's category is %@, which is same as %@.", myApp.name, cat1.name, [[myApp category] name]);    
    NSLog(@"App store has a total of %d apps", [appStore calculateAppCount]);                      
    [pool drain];                                                 
    return 0;
}
