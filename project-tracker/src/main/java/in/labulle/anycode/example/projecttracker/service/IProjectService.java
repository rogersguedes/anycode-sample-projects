package in.labulle.anycode.example.projecttracker.service;

import in.labulle.anycode.example.projecttracker.core.Project;

import java.util.List;

public interface IProjectService {

	public List<Project> findProjectByName(final String name);

}
