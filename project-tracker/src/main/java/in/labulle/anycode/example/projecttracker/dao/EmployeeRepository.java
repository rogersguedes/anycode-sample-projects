package in.labulle.anycode.example.projecttracker.dao;

import in.labulle.anycode.example.projecttracker.core.Employee;

@org.springframework.stereotype.Repository
public interface EmployeeRepository
		extends
		org.springframework.data.jpa.repository.JpaRepository<Employee, java.lang.Long> {

}
