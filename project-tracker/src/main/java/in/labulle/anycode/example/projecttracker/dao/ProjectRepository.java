package in.labulle.anycode.example.projecttracker.dao;

import in.labulle.anycode.example.projecttracker.core.Project;

import java.util.List;

@org.springframework.stereotype.Repository
public interface ProjectRepository
		extends
		org.springframework.data.jpa.repository.JpaRepository<Project, java.lang.Long> {

	public List<Project> findByName(final String name);
	// return type :<Project>

}
