package in.labulle.anycode.example.projecttracker.dao;

import in.labulle.anycode.example.projecttracker.core.Account;

@org.springframework.stereotype.Repository
public interface AccountRepository
		extends
		org.springframework.data.jpa.repository.JpaRepository<Account, java.lang.Long> {

}
