package in.labulle.anycode.example.projecttracker.service.impl;

import in.labulle.anycode.example.projecttracker.core.Project;
import in.labulle.anycode.example.projecttracker.dao.ProjectRepository;
import in.labulle.anycode.example.projecttracker.service.IProjectService;

import java.util.List;

@org.springframework.stereotype.Service
public class ProjectService implements IProjectService {

	@org.springframework.beans.factory.annotation.Autowired
	private ProjectRepository projectRepository;

	public final ProjectRepository getProjectRepository() {
		return this.projectRepository;
	}

	public final void setProjectRepository(
			final ProjectRepository someProjectRepository) {
		this.projectRepository = someProjectRepository;
	}

	public List<Project> findProjectByName(final String name) {
		return projectRepository.findByName(name);
	}

}
