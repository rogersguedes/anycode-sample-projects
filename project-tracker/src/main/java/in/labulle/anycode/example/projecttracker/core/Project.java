package in.labulle.anycode.example.projecttracker.core;

import java.util.Date;

@javax.persistence.Entity
public class Project {
	@javax.persistence.Id
	private java.lang.Long id;

	public final java.lang.Long getId() {
		return this.id;
	}

	public final void setId(final java.lang.Long someId) {
		this.id = someId;
	}

	private String name;

	private Date startDate;

	@javax.persistence.ManyToOne
	private Employee leader;

	@javax.persistence.ManyToMany
	private java.util.List<Employee> members;

	public final String getName() {
		return this.name;
	}

	public final void setName(final String someName) {
		this.name = someName;
	}

	public final Date getStartDate() {
		return this.startDate;
	}

	public final void setStartDate(final Date someStartDate) {
		this.startDate = someStartDate;
	}

	public final Employee getLeader() {
		return this.leader;
	}

	public final void setLeader(final Employee someLeader) {
		this.leader = someLeader;
	}

	public final java.util.List<Employee> getMembers() {
		return this.members;
	}

	public final void setMembers(final java.util.List<Employee> someMembers) {
		this.members = someMembers;
	}

}
