package in.labulle.anycode.example.projecttracker.core;

@javax.persistence.Entity
public class Employee {
	@javax.persistence.Id
	private java.lang.Long id;

	public final java.lang.Long getId() {
		return this.id;
	}

	public final void setId(final java.lang.Long someId) {
		this.id = someId;
	}

	@javax.persistence.OneToMany(mappedBy = "leader")
	private java.util.List<Project> projectsImLeaderOf;

	@javax.persistence.OneToOne
	private Account account;

	@javax.persistence.ManyToMany(mappedBy = "members")
	private java.util.List<Project> projectsImMemberOf;

	public final java.util.List<Project> getProjectsImLeaderOf() {
		return this.projectsImLeaderOf;
	}

	public final void setProjectsImLeaderOf(
			final java.util.List<Project> someProjectsImLeaderOf) {
		this.projectsImLeaderOf = someProjectsImLeaderOf;
	}

	public final Account getAccount() {
		return this.account;
	}

	public final void setAccount(final Account someAccount) {
		this.account = someAccount;
	}

	public final java.util.List<Project> getProjectsImMemberOf() {
		return this.projectsImMemberOf;
	}

	public final void setProjectsImMemberOf(
			final java.util.List<Project> someProjectsImMemberOf) {
		this.projectsImMemberOf = someProjectsImMemberOf;
	}

}
