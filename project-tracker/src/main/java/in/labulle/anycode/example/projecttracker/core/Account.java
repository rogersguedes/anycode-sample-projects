package in.labulle.anycode.example.projecttracker.core;

@javax.persistence.Entity
public class Account {
	@javax.persistence.Id
	private java.lang.Long id;

	public final java.lang.Long getId() {
		return this.id;
	}

	public final void setId(final java.lang.Long someId) {
		this.id = someId;
	}

	@javax.persistence.OneToOne(mappedBy = "account")
	private Employee employee;

	public final Employee getEmployee() {
		return this.employee;
	}

	public final void setEmployee(final Employee someEmployee) {
		this.employee = someEmployee;
	}

}
