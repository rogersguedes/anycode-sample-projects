package in.labulle.anycode.example.projecttracker.service.impl;

import static org.junit.Assert.assertEquals;
import in.labulle.anycode.example.projecttracker.SpringTest;
import in.labulle.anycode.example.projecttracker.core.Project;
import in.labulle.anycode.example.projecttracker.service.IProjectService;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProjectServiceTest extends SpringTest {
	@Autowired
	private IProjectService projectService;

	@Test
	public void testFindByName() {

		List<Project> projects = projectService.findProjectByName("PROJECT1");
		assertEquals(1, projects.size());

	}

}
